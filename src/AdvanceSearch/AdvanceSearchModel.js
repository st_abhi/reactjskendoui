import * as React from "react";
import '@progress/kendo-theme-material/dist/all.css';
import { Button } from '@progress/kendo-react-buttons';
import { TabStrip, TabStripTab, Card, CardBody, CardActions } from '@progress/kendo-react-layout';
import { Dialog, DialogTitleBar, DialogActionsBar, Window } from '@progress/kendo-react-dialogs';
import { TextField, TextareaAutosize, InputLabel } from '@material-ui/core';
import './AdvanceSearchModel.css';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import { Checkbox, RangeSlider, SliderLabel } from "@progress/kendo-react-inputs";
import { DropDownList } from "@progress/kendo-react-dropdowns";
import Dropdown from 'react-dropdown';
import { Badge, BadgeContainer } from "@progress/kendo-react-indicators";
import IconButton from "@material-ui/core/IconButton";
import InputAdornment from "@material-ui/core/InputAdornment";
import SearchIcon from "@material-ui/icons/Search";
// const shapes = ["rectangle"];//"circle",
const shapes = [
    {
        value: "A"
    },
    {
        value: "B"
    },
    {
        value: "C"
    },
    {
        value: "D"
    },
    {
        value: "E"
    },
    {
        value: "F"
    },
    {
        value: "G"
    },
    {
        value: "H"
    },
    {
        value: "I"
    },
    {
        value: "J"
    },
    {
        value: "K"
    },
    {
        value: "L"
    },
    {
        value: "M"
    },
    {
        value: "N"
    },
    {
        value: "O"
    },
    {
        value: "P"
    },
    {
        value: "Q"
    },
    {
        value: "R"
    },
    {
        value: "S"
    },
    {
        value: "T"
    },
    {
        value: "W"
    },
    {
        value: "X"
    },
    {
        value: "Y"
    },
    {
        value: "Z"
    }
];
const useStyles = makeStyles((theme) => ({
    container: {
        display: 'grid',
        gridTemplateColumns: 'repeat(12, 1fr)',
        gridGap: theme.spacing(0),
    },
    container1: {
        display: 'grid',
        gridTemplateColumns: 'repeat(12, 1fr)',
        gridGap: theme.spacing(10),
        marginBottom: "9px"
    },
    container2: {
        display: 'grid',
        gridTemplateColumns: 'repeat(12, 1fr)',
        gridGap: theme.spacing(4),
    },
    paper: {
        padding: "1px 1px 0px 8px",
        // padding: theme.spacing(0.4),
        textAlign: 'center',
        color: theme.palette.text.secondary,
        whiteSpace: 'nowrap',
        // marginBottom: theme.spacing(1),
        marginBottom: "-4px"
    },
    paper2: {
        // padding: "1px 1px 0px 8px",
        padding: theme.spacing(0.4),
        textAlign: 'center',
        color: theme.palette.text.secondary,
        whiteSpace: 'nowrap',
        marginBottom: theme.spacing(1),
        // marginBottom: "-4px"
    },
    divider: {
        margin: "16px 8px 0px 8px"
        // margin: theme.spacing(2, 0),
    },
    divider1: {
        // margin: "16px 8px 0px 8px"
        // margin: theme.spacing(2, 0),
        margin: "32px 0px 24px 0px",
        /* top: 17px; */
        marginTop: "-5px"
    },
    textarea: {
        resize: "both"
    },
    distributortext: {
        background: 'red',
    }
}));
const AdvanceSearModal = () => {
    const [movegroupvisible, setMoveVisible] = React.useState(true);
    const AdvanceSearchtoggleDialog = () => {
        setMoveVisible(!movegroupvisible);
    };
    const [selected, setSelected] = React.useState(0);
    const [position, setPosition] = React.useState("top");

    const handleSelect = (e) => {
        setSelected(e.selected);
    };

    const handleChange = (e) => {
        setPosition(e.value);
    };

    const options = [
        { value: 'name_of_group1', label: 'Name of the group 1' },
        { value: 'name_of_group2', label: 'Name of the group 2' },
        { value: 'name_of_group3', label: 'Name of the group 3' },
        { value: 'name_of_group4', label: 'Name of the group 4' },
    ];


    // const [companyStatus, setSompanyStatus] = React.useState({
    //     value: {
    //         text: "Active",
    //         id: 1,
    //     },
    // });
    const classes = useStyles();
    return (
        <>
            <div className="body">
                <div className="container">
                    <Button className="advance-search-group-item-btn" icon="plus" onClick={AdvanceSearchtoggleDialog}>Advance Search</Button>
                    {!movegroupvisible &&

                        <Dialog title={"Advance Search"} className="advance-search-group-dialog" onClose={AdvanceSearchtoggleDialog}>
                            <Button className="advance-search-save-btn">Save</Button>
                            <Divider className={classes.divider1} />
                            <TabStrip className="advance-search-tab-strip"
                                selected={selected}
                                onSelect={handleSelect}
                                tabPosition={position}>
                                {/* <TabStripTab className="advance-search-tab-strip" title="Paris">
                                    <div>
                                        <p>
                                            Paris is the capital and most populous city of France. It has an area of 105 square kilometres (41 square miles) and a population in 2013 of 2,229,621 within its administrative limits. The city is both a commune and department, and forms the centre and headquarters of the Île-de-France, or Paris Region, which has an area of 12,012 square kilometres (4,638 square miles) and a population in 2014 of 12,005,077, comprising 18.2 percent of the population of France.
                                        </p>
                                    </div>
                                </TabStripTab> */}
                                <TabStripTab title="Category">
                                    <div>
                                        <Typography variant="subtitle1" gutterBottom>
                                            <Grid container spacing={3}>
                                                <Grid item xs={12}>
                                                    <Paper className={classes.paper}>
                                                        <div className="content">
                                                            <div id="acc">
                                                                <div className={classes.container}>
                                                                    <div style={{ gridColumnEnd: 'span 4' }}>
                                                                        <Paper className={classes.paper}>
                                                                            <TextField
                                                                                InputProps={{
                                                                                    endAdornment: (
                                                                                        <InputAdornment>
                                                                                            <IconButton>
                                                                                                <SearchIcon />
                                                                                            </IconButton>
                                                                                        </InputAdornment>
                                                                                    )
                                                                                }}
                                                                                placeholder="Type here to search . . ."
                                                                                className="advance-search-type-search"
                                                                            />
                                                                        </Paper>
                                                                    </div>
                                                                </div>
                                                                <div id="tab1">
                                                                    <div className={classes.container}>
                                                                        <h5 className="dist1">RECENT SEARCHES</h5>
                                                                    </div>
                                                                    <div className={classes.container}>
                                                                        <div style={{ gridColumnEnd: 'span 4' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <div className="k-card-list">
                                                                                    <Card className="abvance-card">
                                                                                        <CardBody className="advance-card-body">
                                                                                            <p>
                                                                                                HP Laptop; 4gb RAM, 256 GB SSD;
                                                                                                Touchscreen; Graphics card; Windows 11
                                                                                            </p>
                                                                                        </CardBody>
                                                                                    </Card>
                                                                                </div>
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 4' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <div className="k-card-list">
                                                                                    <Card className="abvance-card">
                                                                                        <CardBody className="advance-card-body">
                                                                                            <p>
                                                                                                HP Laptop; 4gb RAM, 256 GB SSD;
                                                                                                Touchscreen; Graphics card; Windows 11
                                                                                            </p>
                                                                                        </CardBody>
                                                                                    </Card>
                                                                                </div>
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 4' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <div className="k-card-list">
                                                                                    <Card className="abvance-card">
                                                                                        <CardBody className="advance-card-body">
                                                                                            <p>
                                                                                                HP Laptop; 4gb RAM, 256 GB SSD;
                                                                                                Touchscreen; Graphics card; Windows 11
                                                                                            </p>
                                                                                        </CardBody>
                                                                                    </Card>
                                                                                </div>
                                                                            </Paper>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <Divider className={classes.divider} />
                                                                <div id="tab1" className="sty_table">
                                                                    <div className={classes.container}>
                                                                        <h5 className="dist1">ALL CATEGORIES</h5>
                                                                    </div>
                                                                    <div className={classes.container1}>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                    </div>
                                                                    <div className={classes.container1}>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                    </div>
                                                                    <div className={classes.container1}>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                    </div>
                                                                    <div className={classes.container1}>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                    </div>
                                                                    <div className={classes.container1}>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                    </div>
                                                                    <div className={classes.container1}>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                    </div>
                                                                    <div className={classes.container1}>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                    </div>
                                                                    <div className={classes.container1}>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                    </div>
                                                                    <div className={classes.container1}>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                    </div>
                                                                    <div className={classes.container1}>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                    </div>
                                                                    <div className={classes.container1}>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                    </div>
                                                                    <div className={classes.container1}>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                    </div>
                                                                    <div className={classes.container1}>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                    </div>
                                                                    <div className={classes.container1}>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                    </div>
                                                                    <div className={classes.container1}>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                    </div>
                                                                    <div className={classes.container1}>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                    </div>
                                                                    <div className={classes.container1}>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                    </div>
                                                                    <div className={classes.container1}>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                    </div>
                                                                    <div className={classes.container1}>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </Paper>
                                                </Grid>
                                            </Grid>
                                        </Typography>
                                    </div>
                                </TabStripTab>
                                <TabStripTab title="Subcategory">
                                    <div>
                                        <Typography variant="subtitle1" gutterBottom>
                                            <Grid container spacing={3}>
                                                <Grid item xs={12}>
                                                    <Paper className={classes.paper}>
                                                        <div className="content">
                                                            <div id="acc">
                                                                <div className={classes.container}>
                                                                    <div style={{ gridColumnEnd: 'span 4' }}>
                                                                        <Paper className={classes.paper}>
                                                                            <TextField
                                                                                InputProps={{
                                                                                    endAdornment: (
                                                                                        <InputAdornment>
                                                                                            <IconButton>
                                                                                                <SearchIcon />
                                                                                            </IconButton>
                                                                                        </InputAdornment>
                                                                                    )
                                                                                }}
                                                                                placeholder="Type here to search . . ."
                                                                                className="advance-search-type-search"
                                                                            />
                                                                        </Paper>
                                                                    </div>
                                                                </div>
                                                                <div className="vrt_scroll">
                                                                    <div id="tab1">
                                                                        <div className={classes.container}>
                                                                            <h5 className="dist1">RECENT SEARCHES</h5>
                                                                        </div>
                                                                        <div className={classes.container}>
                                                                            <div style={{ gridColumnEnd: 'span 4' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <div className="k-card-list">
                                                                                        <Card className="abvance-card">
                                                                                            <CardBody className="advance-card-body">
                                                                                                <p>
                                                                                                    HP Laptop; 4gb RAM, 256 GB SSD;
                                                                                                    Touchscreen; Graphics card; Windows 11
                                                                                                </p>
                                                                                            </CardBody>
                                                                                        </Card>
                                                                                    </div>
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 4' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <div className="k-card-list">
                                                                                        <Card className="abvance-card">
                                                                                            <CardBody className="advance-card-body">
                                                                                                <p>
                                                                                                    HP Laptop; 4gb RAM, 256 GB SSD;
                                                                                                    Touchscreen; Graphics card; Windows 11
                                                                                                </p>
                                                                                            </CardBody>
                                                                                        </Card>
                                                                                    </div>
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 4' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <div className="k-card-list">
                                                                                        <Card className="abvance-card">
                                                                                            <CardBody className="advance-card-body">
                                                                                                <p>
                                                                                                    HP Laptop; 4gb RAM, 256 GB SSD;
                                                                                                    Touchscreen; Graphics card; Windows 11
                                                                                                </p>
                                                                                            </CardBody>
                                                                                        </Card>
                                                                                    </div>
                                                                                </Paper>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <Divider className={classes.divider} />
                                                                    <div id="tab1" className="sty_table">
                                                                        <div className={classes.container}>
                                                                            <h5 className="dist1">ALL CATEGORIES</h5>
                                                                        </div>
                                                                        <div className={classes.container1}>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                        </div>
                                                                        <div className={classes.container1}>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                        </div>
                                                                        <div className={classes.container1}>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                        </div>
                                                                        <div className={classes.container1}>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                        </div>
                                                                        <div className={classes.container1}>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                        </div>
                                                                        <div className={classes.container1}>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                        </div>
                                                                        <div className={classes.container1}>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                        </div>
                                                                        <div className={classes.container1}>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                        </div>
                                                                        <div className={classes.container1}>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                        </div>
                                                                        <div className={classes.container1}>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                        </div>
                                                                        <div className={classes.container1}>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                        </div>
                                                                        <div className={classes.container1}>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                        </div>
                                                                        <div className={classes.container1}>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                        </div>
                                                                        <div className={classes.container1}>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                        </div>
                                                                        <div className={classes.container1}>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                        </div>
                                                                        <div className={classes.container1}>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                        </div>
                                                                        <div className={classes.container1}>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                        </div>
                                                                        <div className={classes.container1}>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                        </div>
                                                                        <div className={classes.container1}>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                            <div style={{ gridColumnEnd: 'span 2' }}>
                                                                                <Paper className={classes.paper}>
                                                                                    <Checkbox className="advance-check-box" label={"Category Name"} />
                                                                                </Paper>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </Paper>
                                                </Grid>
                                            </Grid>
                                        </Typography>
                                    </div>
                                </TabStripTab>
                                <TabStripTab title="Brand">
                                    <div>
                                        <Typography variant="subtitle1" gutterBottom>
                                            <Grid container spacing={3}>
                                                <Grid item xs={12}>
                                                    <Paper className={classes.paper}>
                                                        <div className="content">
                                                            <div id="acc">
                                                                <div className={classes.container}>
                                                                    <div style={{ gridColumnEnd: 'span 4' }}>
                                                                        <Paper className={classes.paper}>

                                                                            <TextField
                                                                                InputProps={{
                                                                                    endAdornment: (
                                                                                        <InputAdornment>
                                                                                            <IconButton>
                                                                                                <SearchIcon />
                                                                                            </IconButton>
                                                                                        </InputAdornment>
                                                                                    )
                                                                                }}
                                                                                placeholder="Type here to search . . ."
                                                                                className="advance-search-type-search"
                                                                            />
                                                                        </Paper>
                                                                    </div>

                                                                    <div style={{ gridColumnEnd: 'span 4' }}>
                                                                        <Paper className={classes.paper}>
                                                                            <div className="advance-badge-wrap">
                                                                                {shapes.map((shape, index) => {
                                                                                    console.log("shape", shape)
                                                                                    console.log("index", index)
                                                                                    return (
                                                                                        <BadgeContainer key={index} className="advance-badge-container">
                                                                                            <Badge shape="rectangle">{shape.value}</Badge>
                                                                                        </BadgeContainer>

                                                                                    );
                                                                                })}

                                                                            </div>
                                                                        </Paper>
                                                                    </div>
                                                                </div>
                                                                <div id="tab1">
                                                                    <div className={classes.container}>
                                                                        <h5 className="dist1">RECENT SEARCHES</h5>
                                                                    </div>
                                                                    <div className={classes.container}>
                                                                        <div style={{ gridColumnEnd: 'span 4' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <div className="k-card-list">
                                                                                    <Card className="abvance-card">
                                                                                        <CardBody className="advance-card-body">
                                                                                            <p>
                                                                                                HP Laptop; 4gb RAM, 256 GB SSD;
                                                                                                Touchscreen; Graphics card; Windows 11
                                                                                            </p>
                                                                                        </CardBody>
                                                                                    </Card>
                                                                                </div>
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 4' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <div className="k-card-list">
                                                                                    <Card className="abvance-card">
                                                                                        <CardBody className="advance-card-body">
                                                                                            <p>
                                                                                                HP Laptop; 4gb RAM, 256 GB SSD;
                                                                                                Touchscreen; Graphics card; Windows 11
                                                                                            </p>
                                                                                        </CardBody>
                                                                                    </Card>
                                                                                </div>
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 4' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <div className="k-card-list">
                                                                                    <Card className="abvance-card">
                                                                                        <CardBody className="advance-card-body">
                                                                                            <p>
                                                                                                HP Laptop; 4gb RAM, 256 GB SSD;
                                                                                                Touchscreen; Graphics card; Windows 11
                                                                                            </p>
                                                                                        </CardBody>
                                                                                    </Card>
                                                                                </div>
                                                                            </Paper>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <Divider className={classes.divider} />
                                                                <div id="tab1" className="sty_table">
                                                                    <div className={classes.container}>
                                                                        <h5 className="dist1">ALL BRANDS</h5>
                                                                    </div>
                                                                    <div className={classes.container1}>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                    </div>
                                                                    <div className={classes.container1}>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                    </div>
                                                                    <div className={classes.container1}>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                    </div>
                                                                    <div className={classes.container1}>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                    </div>
                                                                    <div className={classes.container1}>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                    </div>
                                                                    <div className={classes.container1}>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                    </div>
                                                                    <div className={classes.container1}>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                    </div>
                                                                    <div className={classes.container1}>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                    </div>
                                                                    <div className={classes.container1}>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                    </div>
                                                                    <div className={classes.container1}>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                    </div>
                                                                    <div className={classes.container1}>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                    </div>
                                                                    <div className={classes.container1}>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                    </div>
                                                                    <div className={classes.container1}>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                    </div>
                                                                    <div className={classes.container1}>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                    </div>
                                                                    <div className={classes.container1}>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                    </div>
                                                                    <div className={classes.container1}>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                    </div>
                                                                    <div className={classes.container1}>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                    </div>
                                                                    <div className={classes.container1}>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                    </div>
                                                                    <div className={classes.container1}>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                        <div style={{ gridColumnEnd: 'span 2' }}>
                                                                            <Paper className={classes.paper}>
                                                                                <Checkbox className="advance-check-box" label={"Brands Name"} />
                                                                            </Paper>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </Paper>
                                                </Grid>
                                            </Grid>
                                        </Typography>
                                    </div>
                                </TabStripTab>
                                <TabStripTab title="Other Parameters">
                                    <Typography variant="subtitle1" gutterBottom>
                                        <Grid container2 spacing={3}>
                                            <Grid item xs={12}>
                                                <Paper className={classes.paper2}>
                                                    <div className="content">
                                                        <div id="acc">
                                                            <div className={classes.container2}>
                                                                <div style={{ gridColumnEnd: 'span 4' }}>
                                                                    <Paper className={classes.paper2}>
                                                                        <div className="form-level">Catalog</div>
                                                                        <Dropdown options={options} placeholder="Select an option" />
                                                                    </Paper>
                                                                </div>
                                                                <div style={{ gridColumnEnd: 'span 4' }}>
                                                                    <Paper className={classes.paper2}>
                                                                        <div className="form-level">Availability</div>
                                                                        <Dropdown options={options} placeholder="Select an option" />
                                                                    </Paper>
                                                                </div>
                                                            </div>
                                                            <div className={classes.container2}>
                                                                <div style={{ gridColumnEnd: 'span 4' }}>
                                                                    <Paper className={classes.paper2}>
                                                                        <TextField
                                                                            id="name-distributor"
                                                                            label="SKU #"
                                                                            type="text"
                                                                            placeholder="Type here . . ."
                                                                            InputProps={{
                                                                                readOnly: true,
                                                                                className: classes.distributortext
                                                                            }}
                                                                            InputLabelProps={{
                                                                                shrink: true,
                                                                            }}
                                                                        />
                                                                    </Paper>
                                                                </div>
                                                                <div style={{ gridColumnEnd: 'span 4' }}>
                                                                    <Paper className={classes.paper2}>
                                                                        <TextField
                                                                            id="name-distributor"
                                                                            label="SKU #"
                                                                            type="text"
                                                                            placeholder="Type here . . ."
                                                                            InputProps={{
                                                                                readOnly: true,
                                                                                className: classes.distributortext
                                                                            }}
                                                                            InputLabelProps={{
                                                                                shrink: true,
                                                                            }}
                                                                        />
                                                                    </Paper>
                                                                </div>
                                                            </div>
                                                            <div className={classes.container2}>
                                                                <div style={{ gridColumnEnd: 'span 8' }}>
                                                                    <Paper className={classes.paper2}>
                                                                        <TextField
                                                                            id="name-distributor"
                                                                            label="Keywords"
                                                                            type="text"
                                                                            placeholder="Type here . . ."
                                                                            InputProps={{
                                                                                readOnly: true,
                                                                                className: classes.distributortext1
                                                                            }}
                                                                            InputLabelProps={{
                                                                                shrink: true,
                                                                            }}
                                                                        />
                                                                    </Paper>
                                                                </div>
                                                            </div>
                                                            <div className={classes.container2}>
                                                                <div style={{ gridColumnEnd: 'span 4' }}>
                                                                    <Paper className={classes.paper2}>
                                                                        <RangeSlider
                                                                            defaultValue={{
                                                                                start: 30,
                                                                                end: 70,
                                                                            }}
                                                                            step={1}
                                                                            min={0}
                                                                            max={100}
                                                                        >
                                                                            {[0, 25, 50, 75, 100].map((perc, i) => (
                                                                                <SliderLabel key={i} position={perc}>
                                                                                    {perc.toString()}
                                                                                </SliderLabel>
                                                                            ))}
                                                                        </RangeSlider>
                                                                    </Paper>
                                                                </div>
                                                                <div style={{ gridColumnEnd: 'span 2' }}>
                                                                    <Paper className={classes.paper2}>
                                                                        <TextField
                                                                            id="name-distributor"
                                                                            label="Minimum Price"
                                                                            type="text"
                                                                            placeholder="Type here . . ."
                                                                            InputProps={{
                                                                                readOnly: true,
                                                                                className: classes.distributortext
                                                                            }}
                                                                            InputLabelProps={{
                                                                                shrink: true,
                                                                            }}
                                                                        />
                                                                    </Paper>
                                                                </div>
                                                                <div style={{ gridColumnEnd: 'span 2' }}>
                                                                    <Paper className={classes.paper2}>
                                                                        <TextField
                                                                            id="name-distributor"
                                                                            label="Maximum Price"
                                                                            type="text"
                                                                            placeholder="Type here . . ."
                                                                            InputProps={{
                                                                                readOnly: true,
                                                                                className: classes.distributortext
                                                                            }}
                                                                            InputLabelProps={{
                                                                                shrink: true,
                                                                            }}
                                                                        />
                                                                    </Paper>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </Paper>
                                            </Grid>
                                        </Grid>
                                    </Typography>
                                </TabStripTab>
                            </TabStrip>
                        </Dialog>}
                </div>
            </div>
        </>
    );

};
export default AdvanceSearModal;