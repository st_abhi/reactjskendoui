import React from 'react';
import SimpleLineIcon from 'react-simple-line-icons';
import imagePath from '../src/img/laptop.png';
import GroupIcon from '../src/img/Group.png';
import SettingIconImage from '../src/img/setting.png';


export const Image = () => {
  return (
    <td><img src={imagePath} className="Img" /></td>
  );
}

export const Description = () => {
  return (
    <td className="Description">Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum</td>
  );
}

export const Part = () => {
  return (
    <td className="Part">ABCDEF-123456</td>
  );
}

export const Mfr = () => {
  return (
    <td className="Mfr">Apple Inc.</td>
  );
}
export const Price = () => {
  return (
    <td className="Price">$ 1999</td>
  );
}
export const Catalog = () => {
  return (
    <td className="Catalog">Var Street</td>
  );
}
export const EnteredBy = () => {
  return (
    <td className="EnteredBy">Saquib Jawed</td>
  );
}
// Part, Mfr, Price, Catalog, 
export const Sortrder = () => {
  return (
    <td className="Sortrder">
      <select id="select-box">
        <option selected>1.1</option>
        <option>1.2</option>
        <option>1.3</option>
        <option>1.4</option>
        <option>1.5</option>
        <option>1.6</option>
        <option>1.7</option>
        <option>1.8</option>
        <option>1.9</option>
      </select>
    </td>
  );
}
export const SettingIcon = () => {
  return (
    <img src={SettingIconImage} className="SettingIcon" />
  );
}

export const CheckboxIcon = () => {
  return (
    // <td className="checkHeader">
    <input type="checkbox" className="CheckboxIcon" />
    // </td >
  );
}

export const CheckboxInput = ({ mainChecked, sendUncheck }) => {
  const [checked, setChecked] = React.useState(mainChecked);
  const onCellChange = (event) => {
    if (event.target.checked == true) {
      setChecked(true);
    } else {
      setChecked(false);
    }
  };
  return (
    <input type="checkbox" className="Checkbox" onClick={onCellChange} checked={checked} />
  );
}

