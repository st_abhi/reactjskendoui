import React from 'react';
import { Link } from 'react-router-dom';
import { DropDownButton, SplitButton, DropDownButtonItem, SplitButtonItem, ButtonLook, Button } from '@progress/kendo-react-buttons';
import { Menu } from '@progress/kendo-react-layout';

const Sortitems = [
    {
        "text": "Add",
        "items": [
            { "text": "Add Product" },
            { "text": "Add Product Group" },
        ]
    }
];
const Navbar = () => {
    return (
        <div className="navbar">
            <div className="">
                <div className="logo">
                    <a href="#"><strong> Customer Master</strong></a>
                </div>
                <div className="menu-left">
                    <ul className="ulnavbar">

                        {/* <li><Link class="textli" exact to="/account_details">Account</Link></li> */}
                        <li><Link class="textli" exact to="/advance_search">Company</Link></li>
                        <li><Link class="textli" exact to="/account_details">Contacts</Link> </li>
                        <li><a class="textli" href="javascript:void(0);">Address Book</a></li>
                        <li>
                            <Link class="textli active" exact to="/company_favorite">Company Favourite</Link>
                        </li>
                        <li><a class="textli" href="javascript:void(0);">Special Pricing</a></li>
                        <li><a class="textli" href="javascript:void(0);">Credit Cards</a></li>
                        <li><a class="textli" href="javascript:void(0);">Inventory Items</a></li>
                    </ul>
                </div>
                <div className="menu-right">
                    <Menu items={Sortitems} hoverCloseDelay={50000} openOnClick={true} className="navbutton" />
                    {/* <SplitButton text="Add"
                        role="menu" aria-haspopup="true"
                        popupSettings={{
                            popupClass: "my-popup",
                        }}
                        className="navbutton" style={{
                            color: "white"
                        }}>
                        <SplitButtonItem text="Add Product" />
                        <SplitButtonItem text="Add Product Group" />
                    </SplitButton> */}
                </div>

            </div>

        </div >
    )
}

export default Navbar;