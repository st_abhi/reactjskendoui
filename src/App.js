import * as React from "react";
import { HashRouter, Switch, Route, Redirect } from "react-router-dom";
import CompanyFavourite from "./Favourite/CompanyFavourite";
import Navbar from "./Navigation/Navbar";
import AccountDetails from "./AccountDetails/AccountDetails";
import AdvanceSearModal from "./AdvanceSearch/AdvanceSearchModel";
export const App = () => {
    return (
        <>
            <HashRouter>
                <Navbar />
                {/* <HomePage /> */}
                <Switch>
                    <Route exact={true} path="/" component={CompanyFavourite} />
                    <Route exact={true} path="/account_details" component={AccountDetails} />
                    <Route exact={true} path="/advance_search" component={AdvanceSearModal} />
                    <Redirect to="/" />
                </Switch>
            </HashRouter>

        </>
    );
}