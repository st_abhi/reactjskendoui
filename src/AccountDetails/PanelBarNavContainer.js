// import * as React from "react";
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import { DropDownList } from "@progress/kendo-react-dropdowns";
import { Link, animateScroll as scroll } from "react-scroll";
import ImxGroupLogo from '../img/imx-logo.png';
import ImgMenuicon from '../img/m-icon.png';
import Dropdown from 'react-dropdown';
import { Button } from '@progress/kendo-react-buttons';
import { TabStrip, TabStripTab, Card, CardBody, CardActions } from '@progress/kendo-react-layout';
import { Dialog, DialogTitleBar, DialogActionsBar, Window } from '@progress/kendo-react-dialogs';
import { TextField, TextareaAutosize, InputLabel } from '@material-ui/core';
import { Editor, EditorTools } from "@progress/kendo-react-editor";
import content from "./content-overview";

// import { makeStyles } from '@material-ui/core/styles';
import '../AccountDetails/TabBar.css';
const {
  Bold,
  Italic,
  Underline,
  Strikethrough,
  Subscript,
  Superscript,
  AlignLeft,
  AlignCenter,
  AlignRight,
  AlignJustify,
  Indent,
  Outdent,
  OrderedList,
  UnorderedList,
  Undo,
  Redo,
  FontSize,
  FontName,
  FormatBlock,
  // Link,
  Unlink,
  InsertImage,
  ViewHtml,
  InsertTable,
  AddRowBefore,
  AddRowAfter,
  AddColumnBefore,
  AddColumnAfter,
  DeleteRow,
  DeleteColumn,
  DeleteTable,
  MergeCells,
  SplitCell,
} = EditorTools;
const useStyles = makeStyles((theme) => ({
  container: {
    display: 'grid',
    gridTemplateColumns: 'repeat(12, 1fr)',
    gridGap: theme.spacing(0),
  },
  paper: {
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
    whiteSpace: 'nowrap',
    marginBottom: theme.spacing(1),
  },
  divider: {
    margin: "16px 8px 0px 7px"
    // margin: theme.spacing(2, 0),
  },
  textarea: {
    resize: "both"
  },
  distributortext: {
    backgroundColor: " #F4F4F4",
  }
}));
const PanelBarNavContainer = () => {

  // const sizes = ["ImmiX Group", "option1", "option2", "option3", "option4", "option5"];
  const distributor = [
    {
      text: "ImmiX Group",
      id: 1,
    },
    {
      text: "ImmiX Group 1",
      id: 2,
    },
    {
      text: "ImmiX Group 2",
      id: 3,
    },
    {
      text: "ImmiX Group 2",
      id: 4,
    },
  ];

  const [state, setState] = React.useState({
    value: {
      text: "ImmiX Group1",
      id: 1,
    },
  });

  const companyStatusData = [
    {
      text: "Active",
      id: 1,
    },
    {
      text: "Dactive",
      id: 2,
    },
  ];


  const [companyStatus, setSompanyStatus] = React.useState({
    value: {
      text: "Active",
      id: 1,
    },
  });
  const paymentTermData = [
    {
      text: "Custom Catalog",
      id: 1,
    },
  ];

  const [paymentTerm, setpaymentTerm] = React.useState({
    value: {
      text: "Custom Catalog",
      id: 1,
    },
  });
  const shippingData = [
    {
      text: "All",
      id: 1,
    },
  ];

  const [shippingStatus, setshipping] = React.useState({
    value: {
      text: "All",
      id: 1,
    },
  });

  const shippingMethodData = [
    {
      text: "Not applicable",
      id: 1,
    },
  ];

  const [shippingMethodStatus, setMethodShipping] = React.useState({
    value: {
      text: "Not applicable",
      id: 1,
    },
  });
  const purchaseOrderData = [
    {
      text: "--",
      id: 1,
    },
  ];

  const [purchaseOrderStatus, setPurchaseOrder] = React.useState({
    value: {
      text: "--",
      id: 1,
    },
  });
  const [newaddressvisible, setMoveVisible] = React.useState(true);
  const AdvanceSearchtoggleDialog = () => {
    setMoveVisible(!newaddressvisible);
  };
  const [selected, setSelected] = React.useState(0);
  const [position, setPosition] = React.useState("top");

  const handleSelect = (e) => {
    setSelected(e.selected);
  };

  const handleChange = (e) => {
    setPosition(e.value);
  };
  const classes = useStyles();
  const options = [
    { value: '1', label: 'Name' },
    { value: 'name_of_group2', label: 'Name' },
    { value: 'name_of_group3', label: 'Name' },
    { value: 'name_of_group4', label: 'Name' },
  ];


  const dropdownData = [
    { value: 'ImmiX Group' },
    { value: 'ImmiX Group 1' },
    { value: 'ImmiX Group 2' },
    { value: 'ImmiX Group 3' },
  ];
  const dropdownDataStatus = [
    { value: 'Active' },
    { value: 'Dactive' },
  ];
  const dropdownDataPayment = [
    { value: 'Custom Catalog' },
  ];
  const dropdownDataShipping = [
    { value: 'Custom Catalog' },
  ];
  const dropdownDataPreferShipping = [
    { value: 'Not applicable' },
  ];


  return (
    <div className="body">
      <div className="container">
        {!newaddressvisible &&

          <Dialog title={"Add New Address"} className="add-new-address-dialog sty_acc_modal" onClose={AdvanceSearchtoggleDialog}>
            <Divider className={classes.divider1} />
            <TabStrip className="add-new-address-tab-strip"
              selected={selected}
              onSelect={handleSelect}
              tabPosition={position}>
              <TabStripTab title="Address Details">
                <div>
                  <Typography variant="subtitle1" gutterBottom>
                    <Grid container spacing={1}>
                      <Grid item xs={12} id="dialog_pr">
                        <Paper className={classes.paper} id="dialog_ch">
                          <div className={classes.container}>
                            <div style={{ gridColumnEnd: 'span 12' }}>
                              <Paper className={classes.paper}>
                                <TextField
                                  id="name-distributor"
                                  label="Address Line 1"
                                  type="text"
                                  className=""
                                  placeholder="Type here . . ."
                                  // defaultValue="Active"
                                  InputLabelProps={{
                                    shrink: true,
                                  }}

                                />
                              </Paper>
                            </div>
                          </div>
                          <div className={classes.container}>
                            <div style={{ gridColumnEnd: 'span 12' }}>
                              <Paper className={classes.paper}>
                                <TextField
                                  id="name-distributor"
                                  label="Address Line 2"
                                  type="text"
                                  className=""
                                  placeholder="Type here . . ."
                                  // defaultValue="Active"
                                  InputLabelProps={{
                                    shrink: true,
                                  }}

                                />
                              </Paper>
                            </div>
                          </div>
                          <div className={classes.container}>
                            <div style={{ gridColumnEnd: 'span 6' }}>
                              <Paper className={classes.paper}>
                                <div>City</div>
                                <Dropdown options={options} placeholder="Type here . . ." />
                              </Paper>
                            </div>
                            <div style={{ gridColumnEnd: 'span 6' }}>
                              <Paper className={classes.paper}>
                                <div>State</div>
                                <Dropdown options={options} placeholder="Type here . . ." />
                              </Paper>
                            </div>
                          </div>

                          <div className={classes.container}>
                            <div style={{ gridColumnEnd: 'span 6' }}>
                              <Paper className={classes.paper}>
                                <TextField
                                  id="name-distributor"
                                  label="ZIP Code"
                                  type="text"
                                  className=""
                                  placeholder="Type here . . ."
                                  // defaultValue="Active"
                                  InputLabelProps={{
                                    shrink: true,
                                  }}

                                />
                              </Paper>
                            </div>
                            <div style={{ gridColumnEnd: 'span 6' }}>
                              <Paper className={classes.paper}>
                                <div>Country</div>
                                <Dropdown options={options} placeholder="Type here . . ." />
                              </Paper>
                            </div>
                          </div>

                          <div className={classes.container}>
                            <div style={{ gridColumnEnd: 'span 6' }}>
                              <Paper className={classes.paper}>
                                <TextField
                                  id="name-distributor"
                                  label="Phone"
                                  type="text"
                                  className=""
                                  placeholder="Type here . . ."
                                  // defaultValue="Active"
                                  InputLabelProps={{
                                    shrink: true,
                                  }}

                                />
                              </Paper>
                            </div>
                            <div style={{ gridColumnEnd: 'span 6' }}>
                              <Paper className={classes.paper}>
                                <TextField
                                  id="name-distributor"
                                  label="Fax"
                                  type="text"
                                  className=""
                                  placeholder="Type here . . ."
                                  // defaultValue="Active"
                                  InputLabelProps={{
                                    shrink: true,
                                  }}

                                />
                              </Paper>
                            </div>
                          </div>
                          <div id="tab1">
                            <div className={classes.container}>
                              <Button className="add-address-save-btn">Save</Button>
                            </div>
                          </div>
                        </Paper>
                      </Grid>
                    </Grid>
                  </Typography>
                </div>
              </TabStripTab>
              <TabStripTab title="Notes">
                <div>
                  <Typography variant="subtitle1" gutterBottom>
                    <Grid container spacing={3}>
                      <Grid item xs={12}>
                        <Paper className={classes.paper}>
                          <div id="tab1">
                            <div className={classes.container}>
                              <p className="address-line">Address Line 1</p>
                              {/* <h5 className="dist1"></h5> */}
                            </div>
                          </div>
                          <div className={classes.container}>
                            <div style={{ gridColumnEnd: 'span 12' }}>
                              <Paper className={classes.paper}>
                                <Editor
                                  tools={[
                                    [Bold, Italic, Underline, Strikethrough],
                                    [Subscript, Superscript],
                                    [AlignLeft, AlignCenter, AlignRight, AlignJustify],
                                    [Indent, Outdent],
                                    [OrderedList, UnorderedList],
                                    // FontSize,
                                    // FontName,
                                    // FormatBlock,
                                    [Undo, Redo],
                                    [Link, Unlink, InsertImage, ViewHtml],
                                    [InsertTable],
                                    [AddRowBefore, AddRowAfter, AddColumnBefore, AddColumnAfter],
                                    [DeleteRow, DeleteColumn, DeleteTable],
                                    [MergeCells, SplitCell],
                                  ]}
                                  contentStyle={{
                                    height: "240px",
                                    width: "568px"
                                  }}
                                  defaultContent={content}
                                />
                              </Paper>
                            </div>
                          </div>
                          <div id="tab1">
                            <div className={classes.container}>
                              <Button className="add-address-save-btn">Save</Button>
                            </div>
                          </div>
                        </Paper>
                      </Grid>
                    </Grid>
                  </Typography>
                </div>
              </TabStripTab>
            </TabStrip>
          </Dialog>}
        <form className={classes.root} noValidate autoComplete="off">
          <Typography variant="subtitle1" gutterBottom>
            <Grid container spacing={3}>
              <Grid item xs={2}>
                <Paper className={classes.paper}>
                  <div className="sidebar-menu">
                    <p className="section">Sections <img src={ImgMenuicon} className="menu-icon" /></p>
                    <ul>
                      <li>
                        <Link
                          activeClass="active"
                          to="acc"
                          spy={true}
                          smooth={true}
                          offset={-70}
                          duration={500}
                        >
                          Account Details
                        </Link>
                      </li>
                      <li>
                        <Link
                          activeClass="active"
                          to="ftp"
                          spy={true}
                          smooth={true}
                          offset={-70}
                          duration={500}
                        >
                          FTP Details
                        </Link>
                      </li>
                      <li>
                        <Link
                          activeClass="active"
                          to="esr"
                          spy={true}
                          smooth={true}
                          offset={-70}
                          duration={500}
                        >
                          External System Reference
                        </Link>
                      </li>
                      <li>
                        <Link
                          activeClass="active"
                          to="ads"
                          spy={true}
                          smooth={true}
                          offset={-70}
                          duration={500}
                        >
                          Address Details
                        </Link>
                      </li>
                      <li>
                        <Link
                          activeClass="active"
                          to="eltd"
                          spy={true}
                          smooth={true}
                          offset={-70}
                          duration={500}
                        >
                          Lead Time Details
                        </Link>
                      </li>
                    </ul>
                  </div>
                </Paper>
              </Grid>
              <Grid item xs={10}>
                <Paper className={classes.paper}>
                  <div className="content sty_acc_form">
                    <div id="acc">
                      <div className={classes.container}>
                        <div style={{ gridColumnEnd: 'span 3' }}>
                          <Paper className={classes.paper}>
                            <div className="hedding_h5">
                              <h5 className="dist">DISTRIBUTOR</h5>
                              <div className="form-level">Select Distributor</div>
                              <Dropdown className="discount-form-dropdown" options={dropdownData} placeholder="ImmiX Group" />
                            </div>
                          </Paper>
                        </div>
                        <div style={{ gridColumnEnd: 'span 9' }}>
                          <Paper className={classes.paper}>
                            <div className="imc-logo">
                              <img src={ImxGroupLogo} className="imx-logo" />
                            </div>
                          </Paper>
                        </div>

                      </div>




                    </div>
                    <Divider className={classes.divider} />
                    <div id="tab1">
                      <div className={classes.container}>
                        <h5 className="dist pl-2">INTERNET ACCOUNT DETAILS</h5>
                      </div>
                      <div className={classes.container}>
                        <div style={{ gridColumnEnd: 'span 3' }}>
                          <Paper className={classes.paper} data-shrink="true">
                            <TextField

                              id="name-distributor"
                              label="Distributor"
                              type="text"
                              placeholder="Distributor"
                              defaultValue="IMMIX GROUP"
                              InputProps={{
                                readOnly: true,
                                className: classes.distributortext
                              }}
                              InputLabelProps={{
                                shrink: true,
                              }}
                            />
                          </Paper>
                        </div>
                        <div style={{ gridColumnEnd: 'span 3' }}>
                          <Paper className={classes.paper}>
                            <div>

                              <div className="form-level">Company status</div>
                              <Dropdown className="discount-form-dropdown" options={dropdownDataStatus} placeholder="Active" />
                            </div>
                          </Paper>
                        </div>
                        <div style={{ gridColumnEnd: 'span 3' }}>
                          <Paper className={classes.paper}>
                            <TextField
                              id="name-distributor"
                              label="Account number"
                              type="text"
                              className=""
                              placeholder="Account number"
                              defaultValue="QWERTY123456"
                              InputLabelProps={{
                                shrink: true,
                              }}
                            />
                          </Paper>
                        </div>
                        <div style={{ gridColumnEnd: 'span 3' }}>
                          <Paper className={classes.paper}>
                            <TextField
                              id="name-distributor"
                              label="Distributor Suffix"
                              type="text"
                              className=""
                              placeholder="Distributor Suffix"
                              defaultValue="IMXGRP"
                              InputLabelProps={{
                                shrink: true,
                              }}
                            />
                          </Paper>
                        </div>
                      </div>

                      <div className={classes.container}>
                        <div style={{ gridColumnEnd: 'span 3' }}>
                          <Paper className={classes.paper}>
                            <TextField
                              id="name-distributor"
                              label="Flooring A/C #"
                              type="text"
                              className=""
                              placeholder="Flooring A/C #"
                              defaultValue="Saquib Enterprises"
                              InputLabelProps={{
                                shrink: true,
                              }}

                            />
                          </Paper>
                        </div>
                        <div style={{ gridColumnEnd: 'span 3' }}>
                          <Paper className={classes.paper}>
                            <TextField
                              id="name-distributor"
                              label="User ID"
                              type="text"
                              className=""
                              placeholder="User ID"
                              defaultValue="Saquib Enterprises"
                              InputLabelProps={{
                                shrink: true,
                              }}

                            />
                          </Paper>
                        </div>
                      </div>
                    </div>
                    <Divider className={classes.divider} />
                    <div id="ftp">
                      <div className={classes.container}>
                        <h5 className="dist1">FTP ACCOUNT DETAILS</h5>
                      </div>
                      <div className={classes.container}>
                        <div style={{ gridColumnEnd: 'span 3' }}>
                          <Paper className={classes.paper}>
                            <TextField
                              id="name-distributor"
                              label="FTP User Name"
                              type="text"
                              className=""
                              placeholder="FTP User Name"
                              defaultValue="Active"
                              InputLabelProps={{
                                shrink: true,
                              }}

                            />
                          </Paper>
                        </div>
                        <div style={{ gridColumnEnd: 'span 3' }}>
                          <Paper className={classes.paper}>
                            <TextField
                              id="name-distributor"
                              label="FTP Password"
                              type="text"
                              className=""
                              placeholder="FTP Password"
                              defaultValue="QWERTY123456"
                              InputLabelProps={{
                                shrink: true,
                              }}
                            />
                          </Paper>
                        </div>
                        <div style={{ gridColumnEnd: 'span 3' }}>
                          <Paper className={classes.paper}>
                            <TextField
                              id="name-distributor"
                              label="Name of FTP File"
                              type="text"
                              className=""
                              placeholder="Name of FTP File"
                              defaultValue="IMXGRP"
                              InputLabelProps={{
                                shrink: true,
                              }}
                            />
                          </Paper>
                        </div>
                      </div>
                    </div>
                    <Divider className={classes.divider} />
                    <div id="vd">
                      <div className={classes.container}>
                        <h5 className="dist1">VENDOR TERMS</h5>
                      </div>
                      <div className={classes.container}>
                        <div style={{ gridColumnEnd: 'span 3' }}>
                          <Paper className={classes.paper}>
                            <TextField

                              id="name-distributor"
                              label="Vendor Reference ID"
                              type="text"
                              className=""
                              placeholder="Vendor Reference ID"
                              defaultValue="XYZ"
                              InputLabelProps={{
                                shrink: true,
                              }}

                            />
                          </Paper>
                        </div>
                        <div style={{ gridColumnEnd: 'span 3' }}>
                          <Paper className={classes.paper}>

                            <div className="form-level">Default Payment Term Net</div>
                            <Dropdown className="discount-form-dropdown" options={dropdownDataPayment} placeholder="Custom Catalog" />


                            {/*    <div className="form-level">Default Payment Term Net</div>
                            <DropDownList
                              className="modal-dropdown"
                              data={paymentTermData}
                              textField="text"
                              dataItemKey="id"
                              value={paymentTerm.value}
                            />*/}
                          </Paper>
                        </div>
                        <div style={{ gridColumnEnd: 'span 3' }}>
                          <Paper className={classes.paper}>
                            <div className="form-level">Default Shipping Method</div>
                            <Dropdown className="discount-form-dropdown" options={dropdownDataShipping} placeholder="All" />

                            {/*  <DropDownList
                              className="modal-dropdown"
                              data={shippingData}
                              textField="text"
                              dataItemKey="id"
                              value={shippingStatus.value}
                            />*/}
                          </Paper>
                        </div>
                        <div style={{ gridColumnEnd: 'span 3' }}>
                          <Paper className={classes.paper}>
                            <div className="form-level">Prefer Default Shipping Method</div>
                            <Dropdown className="discount-form-dropdown" options={dropdownDataPreferShipping} placeholder="Not applicable" />
                            {/*<DropDownList
                              className="modal-dropdown"
                              data={shippingMethodData}
                              textField="text"
                              dataItemKey="id"
                              value={shippingMethodStatus.value}
                            />*/}
                          </Paper>
                        </div>
                      </div>
                      <div className={classes.container}>
                        <div id="txt-cl" style={{ gridColumnEnd: 'span 6' }}>
                          <Paper className={classes.paper}>
                            <div className="tst-label"> Registration domains</div>
                            <TextField
                              id="outlined-textarea"
                              placeholder="Registration domains"
                              multiline
                              variant="outlined"
                              className="txt-ml"
                              defaultValue="@gmail.com, @varstreet.com"
                              // helperText="Enter multiple email IDs separated by semi-colon (;) 0/220"
                              rows={5}
                              inputProps={{ className: classes.textarea }}
                            />
                          </Paper>
                          <div id="txt-bott" className={classes.container}>
                            <div style={{ gridColumnEnd: 'span 6' }}>
                              <Paper className={classes.paper}>
                                <p className="txt-p">Enter multiple email IDs separated by semi-colon (;)</p>
                              </Paper>
                            </div>
                            <div style={{ gridColumnEnd: 'span 6' }}>
                              <Paper className={classes.paper}>
                                <p className="txt-right">0/220</p>
                              </Paper>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <Divider className={classes.divider} />
                    <div id="esr">
                      <div className={classes.container}>
                        <h5 className="dist1">EXTERNAL SYSTEM REFERENCE</h5>
                      </div>
                      <div className={classes.container}>
                        <div style={{ gridColumnEnd: 'span 3' }}>
                          <Paper className={classes.paper}>
                            <TextField
                              id="name-distributor"
                              label="CRM Reference Number"
                              type="text"
                              className=""
                              placeholder="CRM Reference Number"
                              defaultValue="ABCD123456"
                              helperText="Unique reference number for the customer in CRM system"
                              InputLabelProps={{
                                shrink: true,
                              }}

                            />
                          </Paper>
                        </div>
                        <div style={{ gridColumnEnd: 'span 3' }}>
                          <Paper className={classes.paper}>
                            <TextField
                              id="name-distributor"
                              label="CRM Reference Number"
                              type="text"
                              className=""
                              placeholder="Type Here........"
                              helperText="Unique reference number for the customer in CRM system"
                              InputLabelProps={{
                                shrink: true,
                              }}
                            />
                          </Paper>
                        </div>
                        <div style={{ gridColumnEnd: 'span 3' }}>
                          <Paper className={classes.paper}>
                            <TextField
                              id="name-distributor"
                              label="CRM Reference Number"
                              type="text"
                              className=""
                              placeholder="Type Here........"
                              helperText="Unique reference number for the customer in CRM system"
                              InputLabelProps={{
                                shrink: true,
                              }}
                            />
                          </Paper>
                        </div>
                        <div style={{ gridColumnEnd: 'span 3' }}>
                          <Paper className={classes.paper}>
                            <TextField
                              id="name-distributor"
                              label="CRM Reference Number"
                              type="text"
                              className=""
                              placeholder="Type Here........"
                              helperText="Unique reference number for the customer in CRM system"
                              InputLabelProps={{
                                shrink: true,
                              }}
                            />
                          </Paper>
                        </div>
                      </div>
                    </div>
                    <Divider className={classes.divider} />
                    <div id="ads">
                      <div className={classes.container}>
                        <h5 className="dist1">ADDRESS DETAILS </h5>
                      </div>
                      <div className={classes.container}>
                        <div style={{ gridColumnEnd: 'span 6' }}>
                          <Paper className={classes.paper}>
                            <div id="ads-br" className={classes.container}>
                              <div style={{ gridColumnEnd: 'span 6' }}>
                                <Paper className={classes.paper}>
                                  <div className="add-left">
                                    <h5 className="dist">Name of the address
                                    </h5>
                                    <p>Bill Company name
                                      Contact first name + Contact last name

                                    </p>
                                    <p>Phone number 1, Fax number 1
                                      Email ID 1, Email ID 2

                                    </p>
                                    <p>
                                      <Button className="address-button" onClick={AdvanceSearchtoggleDialog} primary={true} look="flat">Add New Address</Button>
                                      {/* <Button className="advance-search-group-item-btn" icon="plus">Add New Address</Button> */}
                                    </p>
                                  </div>
                                </Paper>
                              </div>
                              <div style={{ gridColumnEnd: 'span 6' }}>
                                <Paper className={classes.paper}>
                                  <div className="add-rigth">

                                    <p> Address line 1, Address Line 2, Address line 3,
                                      City, State, Country, ZIP Code, Address reference

                                    </p>
                                    <p>TAX ID
                                      TAX exempted - Yes/No (if no, Value)

                                    </p>
                                  </div>
                                </Paper>
                              </div>
                            </div>
                          </Paper>
                        </div>
                      </div>
                    </div>
                    <Divider className={classes.divider} />
                    <div id="eltd">
                      <div className={classes.container}>
                        <h5 className="dist1">ESTIMATED LEAD TIME DETAILS</h5>
                      </div>
                      <div className={classes.container}>
                        <div style={{}}>
                          <Paper className={classes.paper}>
                            <div className="form-level">Purchase Order Placement Cutoff Time (PST)</div>
                            <div className="full-row ">

                              <DropDownList
                                data={purchaseOrderData}
                                textField="text"
                                dataItemKey="id"
                                className="custom-dropdown"
                                value={purchaseOrderStatus.value}
                              />
                              <TextField
                                id="name-distributor"
                                type="text"
                                className="custom-text-area custom-text-area-boder-non"
                                placeholder="CRM Reference Number"

                              />

                              <div className="pa0">
                                <DropDownList
                                  data={purchaseOrderData}
                                  textField="text"
                                  dataItemKey="id"
                                  className="custom-dropdown br-n"
                                  value={purchaseOrderStatus.value}
                                />
                                <TextField
                                  id="name-distributor"
                                  type="text"
                                  className="custom-text-area"
                                  placeholder="CRM Reference Number"

                                />
                              </div>
                            </div>
                          </Paper>
                        </div>
                        <div style={{ gridColumnEnd: 'span 3' }}>
                          <Paper className={classes.paper}>
                            <div className="form-level">Lead Time For Distributor (Business Days)</div>
                            <DropDownList
                              className="modal-dropdown"
                              data={purchaseOrderData}
                              textField="text"
                              dataItemKey="id"
                              value={purchaseOrderStatus.value}
                            />
                          </Paper>
                        </div>
                      </div>
                    </div>
                  </div>
                </Paper>
              </Grid>
            </Grid>
          </Typography>

        </form>

      </div>

    </div>

  );
};
export default PanelBarNavContainer;