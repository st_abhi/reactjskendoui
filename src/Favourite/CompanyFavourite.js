import React, { Component } from 'react';
import '@progress/kendo-theme-material/dist/all.css';
import '../Favourite/Favorite.css';
import '../Favourite/ColumnChosser.css';
import '../Favourite/DiscountRule.css';
import products from '../products.json';
import { Grid, GridColumn, getSelectedState } from "@progress/kendo-react-grid";
// ../src/img / search.png
import SerachIcon from '../img/search.png';
import GroupIcon from '../img/Group.png';
import imagePath from '../img/laptop.png';
import Rectangle2 from '../img/Rectangle2.png';
import CloseImg from '../img/close.png';
import Downarrow from '../img/down-arrow.png';
import SettingIconImage from '../img/setting.png';
import Sort from '../img/sort.png';
import Filter from '../img/filter.png';

import { CheckboxInput } from '../Components';
import { Dialog, DialogActionsBar, Window } from '@progress/kendo-react-dialogs';
import { DropDownButton, SplitButton, DropDownButtonItem, SplitButtonItem, ButtonLook, Button } from '@progress/kendo-react-buttons';
import { Popup, PopupPropsContext } from '@progress/kendo-react-popup';
import Dropdown from 'react-dropdown';
import { DropDownList } from "@progress/kendo-react-dropdowns";
import { Checkbox } from "@progress/kendo-react-inputs";
import 'react-dropdown/style.css';
import { getter } from '@progress/kendo-react-common';

import Divider from '@material-ui/core/Divider';
import { makeStyles } from '@material-ui/core/styles';


import { Menu, MenuItem } from '@progress/kendo-react-layout';

const DATA_ITEM_KEY = 'ProductID';
const SELECTED_FIELD = 'selected';
const idGetter = getter(DATA_ITEM_KEY);
const useStyles = makeStyles((theme) => ({
    divider: {
        margin: theme.spacing(2, 0),
        width: "107%"
    },
}));
const HomePage = () => {

    const [visible, setVisible] = React.useState(true);
    const [checked, setChecked] = React.useState(false);
    const [movegroupvisible, setMoveVisible] = React.useState(true);
    const [groupnamevisible, setGroupVisible] = React.useState(false);
    var checkCount = "All";
    const mainCheckBox = () => {
        setChecked(!checked);
        setVisible(!visible);
        checkCount = "All";
    };
    const movetoggleDialog = () => {
        setMoveVisible(!movegroupvisible);
        setGroupVisible(false);
    };
    const hadleOptionChange = (e) => {
        if (e.value == "add_new_group") {
            setGroupVisible(true);
        } else {
            setGroupVisible(false);
        }

    }
    const [dataState, setDataState] = React.useState(products.map(dataItem => Object.assign({
        selected: false
    }, dataItem)));

    const [selectedState, setSelectedState] = React.useState({});

    const onSelectionChange = React.useCallback(event => {
        const checkboxElement = event.syntheticEvent.target;
        const checked = checkboxElement.checked;
        const newSelectedState = getSelectedState({
            event,
            selectedState: selectedState,
            dataItemKey: DATA_ITEM_KEY
        });
        setSelectedState(newSelectedState);
        setVisible(!checked);

    }, [selectedState]);

    const onHeaderSelectionChange = React.useCallback(event => {
        const checkboxElement = event.syntheticEvent.target;
        const checked = checkboxElement.checked;
        const newSelectedState = {};
        event.dataItems.forEach(item => {
            newSelectedState[idGetter(item)] = checked;
        });
        if (checked == true) {
            setVisible(!visible);
        } else {
            setVisible(visible);
        }
        setSelectedState(newSelectedState);
    }, []);
    const onSelectionClear = () => {
        setVisible(!visible);
        setSelectedState(false);
    }
    const [alert, setAlert] = React.useState("");
    const [displayModal, setDisplayModal] = React.useState(false);
    const [displayColumnModal, setColumnDisplayModal] = React.useState(false);

    const ColumnChooser = () => {
        setColumnDisplayModal(!displayColumnModal)
    }
    const DiscountRule = () => {
        setDisplayModal(!displayModal)

    }
    React.useEffect(() => {
        const clearMessage = setTimeout(() => {
            setAlert("");
        }, 5000);
        return () => clearTimeout(clearMessage);
    }, [alert]);
    // const Sortitems = [
    //     "Product Title (A - Z)",
    //     "Product Title (Z - A)",
    //     "Creaction Date (Older First)",
    //     "Creaction Date (Newest First)",
    //     "Price (Low to High)",
    //     "Price (High to Low)"
    // ];
    const Sortitems = [
        {
            "text": "Sort",
            "items": [
                { "text": "Product Title (A - Z)" },
                { "text": "Product Title (Z - A)" },
                { "text": "Creaction Date (Older First)" },
                { "text": "Creaction Date (Newest First)" },
                { "text": "Price (Low to High)" },
                { "text": "PPrice (High to Low)" },
            ]
        }
    ];
    const wrapper = React.useRef(null);
    const anchor = React.useRef(null);
    const [show, setShow] = React.useState(false);

    const wrapper1 = React.useRef(null);
    const anchor1 = React.useRef(null);
    const [show1, setShow1] = React.useState(false);
    const anchor2 = React.useRef(null);
    const [show2, setShow2] = React.useState(false);

    const anchor3 = React.useRef(null);
    const [show3, setShow3] = React.useState(false);

    const getSort = () => {
        setShow1(false)
        setShow(false)
        setShow2(false)
        setShow3(!show3)
    };
    const onClick = () => {
        setShow1(!show1)
        setShow(false)
        setShow2(false)
        setShow3(false)
    };
    const getFilter = () => {
        setShow(!show)
        setShow1(false)
        setShow3(false)


    }
    const showAddFilter = () => {
        setShow2(true)
        setShow(false)
        setShow1(false)
        setShow3(false)



    }
    const sortFilterOn = () => {
        setShow2(false)
        setShow(false)
        setShow1(false)
    }
    const pageoffset = {
    };
    const filteroffset = {
    };
    const editoffset = {
    };
    const items = [
        {
            text: "page",
            width: "100px",
        },
        {
            text: "1 2 3 4...20 21 22 23 24",
            width: "100px",
        },
        {
            text: "Show Result",
            width: "100px",
        },

    ];

    const options = [
        { value: 'default', label: 'Default', className: "default-selected" },
        { value: 'name_of_group1', label: 'Name of the group 1' },
        { value: 'name_of_group2', label: 'Name of the group 2' },
        { value: 'name_of_group3', label: 'Name of the group 3' },
        { value: 'name_of_group4', label: 'Name of the group 4' },
        { value: '.', label: '', disabled: true, className: 'myOptiondivider' },
        { value: 'add_new_group', label: '+ Add New Group', className: 'myOptionClassName' },
    ];
    const dropdownData = [
        { value: '1', label: 'Select an option . . .' },
        // {
        //     text: "Select an option . . .",
        //     id: 1,
        // },
    ];

    const [state, setState] = React.useState({
        value: {
            text: "Select an option . . .",
            id: 1,
        },
    });
    const SettingIcon = () => {
        return (
            // onClick = { setColumnDisplayModal(!displayColumnModal) }
            <img src={SettingIconImage} className="SettingIcon" onClick={ColumnChooser} />
        );
    }
    const classes = useStyles();
    return (

        <div className="body" >
            <div className="body-container" style={{ "margin-bottom": "25px" }}>
                {!movegroupvisible && <Dialog title={"Select Group"} className="select-group-dialog" onClose={movetoggleDialog}
                >
                    <div style={{ "margin-bottom": "2px" }}>
                        <span className="you-can-create-new-group-or-mo">You can create new group or move the selected items to an existing group. </span>
                    </div>
                    <div style={{ "margin-bottom": "20px" }}>
                        <span className="you-can-create-new-group-or-mo1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </span>
                    </div>
                    <div>
                        <label className="select-group">Select group</label><br />
                        <div>
                            <Dropdown options={options} className="group-button" onChange={hadleOptionChange} placeholder="Default" />
                        </div><br />
                    </div>
                    {groupnamevisible
                        ? <div>
                            <label className="enter-group-name">Enter Group Name</label><br />
                            <div className="group-control">
                                <input type="text" className="type-here" placeholder="type-here..."></input>
                            </div>
                        </div>
                        : <></>
                    }
                    <br />
                    <div>
                        <Button style={{
                            "height": "32px",
                            "width": "112px",
                            "border-radius": "3px",
                            "color": "#FFF",
                            "background-color": "#2666E3"
                        }}>
                            Move to Group
                        </Button>
                    </div>
                </Dialog>}
                {
                    visible &&
                    <div className="search-box">
                        <div className="SerachImg">
                            <img src={SerachIcon} className="SearchImg" />
                        </div>
                        <div className="form-group">
                            <input type="text" placeholder="Search products......" className="form-control" />
                        </div>
                        <div className="Button">
                            <div ref={wrapper1}>
                                <button className='k-button pagination-div page-icon' ref={anchor1} onClick={onClick}>
                                    <span className="sp-fl"> 0 - 20 of 2000 </span> <span className="span-img"><img src={Downarrow} className="down-arrow" /> </span>
                                </button>
                                {setShow1 &&
                                    <Popup offset={pageoffset} show={show1} ref={anchor} onClick={() => setShow1(!show1)} popupClass={'popup-content  pagination-border'} style={{
                                        width: "300px",
                                        boxShadow: 'none',
                                        top: "124px",
                                        right: "160px"
                                    }}>
                                        <div style={{
                                            margin: "14px 0px 104px 19px"
                                        }}>
                                            <p className="page">Page</p>
                                            <div>
                                                <span className="page-span"><a href="javascript:void(0);" className="numb-link">1</a></span>
                                                <span className="page-span"><a href="javascript:void(0);" className="numb-link">2</a></span>
                                                <span className="page-span"><a href="javascript:void(0);" className="numb-link">3</a></span>
                                                <span className="page-span"><a href="javascript:void(0);" className="numb-link">4</a></span>
                                                <span><a href="javascript:void(0);" className="pagedot">.......</a></span>
                                                <span className="page-span"><a href="javascript:void(0);" className="numb-link">20</a></span>
                                                <span className="page-span"><a href="javascript:void(0);" className="numb-link">21</a></span>
                                                <span className="page-span"><a href="javascript:void(0);" className="numb-link">22</a></span>
                                                <span className="page-span"><a href="javascript:void(0);" className="numb-link">23</a></span>
                                                <span className="page-span"><a href="javascript:void(0);" className="numb-link">24</a></span>
                                            </div>
                                            <div className="div-image"><img src={Rectangle2} className="rectangle2" /></div>
                                            <div className="box-btn">
                                                <div className="box1">
                                                    <p className="show-results ">Show Result</p>
                                                    <select id="select-box" className="show-results1">
                                                        <option className="Dropdown-option" selected>20</option>
                                                        <option>10</option>
                                                    </select>
                                                </div>
                                                <div className="box2">
                                                    <p className="go-to">Go To</p>
                                                    <select id="select-box" className="show-results1">
                                                        < option selected>Select...</option>
                                                        <option>10</option>
                                                    </select>
                                                </div>
                                            </div>
                                            < div >
                                            </div>
                                        </div>
                                    </Popup>
                                }
                           
                                {/* <DropDownButton text="Sort" className="Sort" imageUrl={Sort} onFocus={sortFilterOn} items={Sortitems} /> */}
                                <div className="pagination" ref={wrapper}>
                                    <button className='k-button filter-button-div' ref={anchor} onClick={getFilter}>
                                        <span><img src={Filter} className="sort-filter" /></span> filter
                                    </button>
                                    {setShow &&
                                        <Popup offset={filteroffset} show={show} ref={anchor} onClick={() => setShow(!show)} popupClass={'popup-content filter-offset'} style={{
                                            position: "absolute",
                                            height: "132px",
                                            width: "295px",
                                            top: "121px",
                                            left: "inherit",
                                            right: "40px"
                                        }}>
                                            <div className="div-filter" >
                                                <p className="no-filter-selected">No Filter Selected</p>
                                                <p className="filter-p">  <a onClick={showAddFilter} ref={anchor2} href="javascript:void(0);" className="add-filter">Add Filter</a></p>
                                            </div>
                                        </Popup>
                                    }
                                    {setShow2 &&
                                        <Popup offset={editoffset} show={show2} ref={anchor2} onClick={() => setShow2(!show2)} popupClass={'popup-content add-f add-filter-content'} style={{
                                            width: "525px",
                                            right: "85px",
                                            left: "inherit !important",
                                            top: "124px"
                                        }}>
                                            <div>
                                                <table className="filter-table">

                                                    <tbody className="filter-tbody">
                                                        <tr>
                                                            <td><SplitButton className="filter-button-split" text="Status" items={items} /></td>
                                                            <td><SplitButton className="filter-button-split" text="Is" items={items} /></td>
                                                            <td><SplitButton className="filter-button-split" text="Active" items={items} /></td>
                                                            <td className="close"><img src={CloseImg} className="close-img"></img></td>
                                                            <td className="or-button"><button className="td-button or-bord">OR</button> <button className="td-button">AND</button></td>
                                                        </tr>
                                                        <span className="or">OR</span>
                                                        <tr>
                                                            <td><SplitButton className="filter-button-split" text="Select" items={items} /></td>
                                                            <td><SplitButton className="filter-button-split" text="Select" items={items} /></td>
                                                            <td><SplitButton className="filter-button-split" text="Select" items={items} /></td>
                                                            <td className="close"><img src={CloseImg} className="close-img"></img></td>
                                                            <td className="or-button"><button className="td-button or-bord">OR</button> <button className="td-button">AND</button></td>
                                                        </tr>
                                                        <tr className="border-and">
                                                            <td colSpan="2"><span className="and">AND</span></td>
                                                            <td colSpan="4"></td>
                                                        </tr>

                                                        <tr>
                                                            <td><SplitButton className="filter-button-split" text="Select" items={items} /></td>
                                                            <td><SplitButton className="filter-button-split" text="Select" items={items} /></td>
                                                            <td><SplitButton className="filter-button-split" text="Select" items={items} /></td>
                                                            <td className="close"><img src={CloseImg} className="close-img"></img></td>
                                                            <td className="or-button"><button className="td-button or-bord">OR</button> <button className="td-button">AND</button></td>
                                                        </tr>
                                                        <span className="or">OR</span>

                                                        <tr>
                                                            <td><SplitButton className="filter-button-split" text="Select" items={items} /></td>
                                                            <td><SplitButton className="filter-button-split" text="Select" items={items} /></td>
                                                            <td><SplitButton className="filter-button-split" text="Select" items={items} /></td>
                                                            <td><img src={CloseImg} className="close-img"></img></td>
                                                            <td className="or-button"><button className="td-button or-bord">OR</button> <button className="td-button">AND</button></td>
                                                        </tr>
                                                        <span className="or">OR</span>

                                                        <tr>
                                                            <td><SplitButton className="filter-button-split" text="Select" items={items} /></td>
                                                            <td><SplitButton className="filter-button-split" text="Select" items={items} /></td>
                                                            <td><SplitButton className="filter-button-split" text="Select" items={items} /></td>
                                                            <td className="close"><img src={CloseImg} className="close-img"></img></td>
                                                            <td className="or-button"><button className="td-button or-bord">OR</button> <button className="td-button">AND</button></td>
                                                        </tr>
                                                        <tr>
                                                            <td className="add-filter-clear-filters" colSpan="6">
                                                                <a href="javascript:void(0);">Add Filter</a> | <a href="javascript:void(0);">Clear Filter</a></td>
                                                        </tr>

                                                    </tbody>

                                                    <tfoot className="filter-tfoot">
                                                        <tr>
                                                            <td className="close-filter" onClick={sortFilterOn} onBlur={sortFilterOn}>Close</td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td className="close-filter sty_apply">Apply</td>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </Popup>
                                    }
                                </div>


                                  <div className="Sort" ref={wrapper}>
                                    <button className='k-button filter-button-div' ref={anchor} onClick={getSort}>
                                        <span><img src={Sort} className="sort-filter" /></span> Sort
                                    </button>
                                    {setShow3 &&
                                        <Popup offset={filteroffset} show={show3} ref={anchor} onClick={() => setShow3(!show3)} popupClass={'popup-content filter-offset'} style={{
                                            position: "absolute",
                                            height: "132px",
                                            top: "121px",
                                            left: "inherit",
                                            right: "117px",
                                            width: "197px"
                                        }}>
                                            <div className="div-Sort">
                                                 <ul>
                                                    <li>Product Title (A - Z)</li>
                                                    <li>Product Title (Z - A)</li>
                                                    <li>Creaction Date (Older First)</li>
                                                    <li>Creaction Date (Newest First)</li>
                                                    <li>Price (Low to High)</li>
                                                    <li>PPrice (High to Low)</li>
                                                </ul>
                                            </div>
                                        </Popup>
                                    }
                                </div>





                            </div>
                        </div>
                    </div>
                }
                {
                    !visible &&
                    <div className="group-item">

                        <div className="selected-item">
                            <p className="" style={{ "fontSize": "12px" }}>{checkCount} item selected.   <a onClick={onSelectionClear} className="clearlink">clear selection</a> </p>
                        </div>
                        <div className="button-group">
                            <div className="Button">
                                <Button className="group-item-btn" icon="plus" onClick={movetoggleDialog}>Move to group</Button>
                                <Button className="group-item-btn" icon="delete">Delete</Button>
                                <Button className="group-item-btn" icon="download">Export</Button>
                            </div>
                        </div>
                    </div>
                }
                <div>
                    {/* Discount Rule */}
                    <div className="App">
                        <div className={`Modal ${displayModal ? "Show" : ""}`}>
                            <div className="toggle-heading">
                                <span className="add-discount-rule">Add Discount Rule</span>
                                <button
                                    className="Close"
                                    onClick={() => setDisplayModal(!displayModal)}
                                >
                                    X
                                </button>
                            </div>
                            <div className="right-discount-form">

                                <form action="#">
                                    <div className="dis-form secons-dis-form">
                                        <div className="row">
                                            <div className="col-6">
                                                <label for="country">Rule Type</label>
                                                <Dropdown className="discount-form-dropdown" options={dropdownData} placeholder="Select an option . . ." />
                                                {/* <DropDownList
                                                    data={dropdownData}
                                                    className="modal-dropdown"
                                                    textField="text"
                                                    dataItemKey="id"
                                                    value={state.value}
                                                /> */}

                                            </div>
                                        </div>

                                        <div className="col-12">
                                            <div class="form-group">
                                                <label for="fname">Rule Description</label>
                                                <input type="text" id="fname" name="firstname" placeholder="Type here . . ."></input>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-6">
                                                <label for="country">Pricelist</label>
                                                <Dropdown className="discount-form-dropdown" options={dropdownData} placeholder="Select an option . . ." />
                                                {/* <DropDownList
                                                    className="modal-dropdown"
                                                    data={dropdownData}
                                                    className="modal-dropdown"
                                                    textField="text"
                                                    dataItemKey="id"
                                                    value={state.value}
                                                /> */}

                                            </div>
                                            <div className="col-6">
                                                <div class="form-group">
                                                    <label for="country">Calculation Basis </label>

                                                    <Dropdown className="discount-form-dropdown" options={dropdownData} placeholder="Select an option . . ." />
                                                    {/* <DropDownList
                                                        className="modal-dropdown"
                                                        data={dropdownData}
                                                        className="modal-dropdown"
                                                        textField="text"
                                                        dataItemKey="id"
                                                        value={state.value}
                                                    /> */}

                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-6">
                                                <label for="country">Order Value From</label>
                                                <Dropdown className="discount-form-dropdown" options={dropdownData} placeholder="Select an option . . ." />
                                                {/* <DropDownList
                                                    className="modal-dropdown"
                                                    data={dropdownData}
                                                    className="modal-dropdown"
                                                    textField="text"
                                                    dataItemKey="id"
                                                    value={state.value}
                                                /> */}

                                            </div>
                                            <div className="col-6">
                                                <div class="form-group">
                                                    <label for="country">Order Value To</label>
                                                    <Dropdown className="discount-form-dropdown" options={dropdownData} placeholder="Select an option . . ." />
                                                    {/* <DropDownList
                                                        className="modal-dropdown"
                                                        data={dropdownData}
                                                        className="modal-dropdown"
                                                        textField="text"
                                                        dataItemKey="id"
                                                        value={state.value}
                                                    /> */}

                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-6">
                                                <label for="country">Margin Value From (%)</label>
                                                <Dropdown className="discount-form-dropdown" options={dropdownData} placeholder="Select an option . . ." />
                                                {/* <DropDownList
                                                    className="modal-dropdown"
                                                    data={dropdownData}
                                                    className="modal-dropdown"
                                                    textField="text"
                                                    dataItemKey="id"
                                                    value={state.value}
                                                /> */}

                                            </div>
                                            <div className="col-6">
                                                <div class="form-group">
                                                    <label for="country">Margin Value To (%)</label>
                                                    <Dropdown className="discount-form-dropdown" options={dropdownData} placeholder="Select an option . . ." />
                                                    {/* <DropDownList
                                                        className="modal-dropdown"
                                                        data={dropdownData}
                                                        className="modal-dropdown"
                                                        textField="text"
                                                        dataItemKey="id"
                                                        value={state.value}
                                                    /> */}

                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-6">
                                                <label for="country">Effective From</label>
                                                <Dropdown className="discount-form-dropdown" options={dropdownData} placeholder="Select an option . . ." />
                                                {/* <DropDownList
                                                    className="modal-dropdown"
                                                    data={dropdownData}
                                                    className="modal-dropdown"
                                                    textField="text"
                                                    dataItemKey="id"
                                                    value={state.value}
                                                /> */}

                                            </div>
                                            <div className="col-6">
                                                <div class="form-group">
                                                    <label for="country">Effective To</label>
                                                    <Dropdown className="discount-form-dropdown" options={dropdownData} placeholder="Select an option . . ." />
                                                    {/* <DropDownList
                                                        className="modal-dropdown"
                                                        data={dropdownData}
                                                        className="modal-dropdown"
                                                        textField="text"
                                                        dataItemKey="id"
                                                        value={state.value}
                                                    /> */}

                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-6">
                                                <label for="country">Value</label>
                                                <input type="text" id="fname" name="firstname" placeholder="Type here . . ."></input>
                                            </div>
                                            <div className="col-6">

                                                <div class="form-group">
                                                    <label for="country">Minimum Margin</label>
                                                    <div className="sl-cl">
                                                        <div className="sel-left">
                                                            <select id="per-sel" name="country">
                                                                <option value="australia">%</option>
                                                                <option value="canada"> 1</option>
                                                                <option value="usa"> 2</option>
                                                            </select>
                                                        </div>
                                                        <div className="sel-right">
                                                            <select id="country" name="country">
                                                                <option value="australia">Select an option . . .</option>
                                                                <option value="canada">Select an option 1</option>
                                                                <option value="usa">Select an option 2</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="model-bottom">
                                        <input type="submit" value="SAVE"></input>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div
                            className={`Overlay ${displayModal ? "Show" : ""}`}
                            onClick={() => setDisplayModal(!displayModal)}
                        />
                    </div>
                    {/* end Discount rule */}
                    {/* Column chosser */}
                    <div className="App">
                        <div className={`column-chosser ${displayColumnModal ? "Show" : ""}`}>
                            <div className="toggle-heading">
                                <span className="add-discount-rule">Column Chooser</span>
                                {/* <button
                                    className="Close"
                                    onClick={() => setColumnDisplayModal(!displayColumnModal)}
                                >
                                    X
                                </button> */}
                            </div>
                            <div className="right-discount-form">

                                <form action="#">
                                    <div className="dis-form">
                                        <div className="col-12">
                                            <div class="form-group">
                                                <input type="text" id="fname" name="firstname" placeholder="Type here . . ."></input>
                                            </div>
                                        </div>

                                        <Divider className={classes.divider} />
                                        <div className="row row-bottom">
                                            <div className="col-12 row-col-md">
                                                <label for="fname">VISIBLE COLUMNS</label>
                                            </div>
                                        </div>
                                        <div className="row row-check-box">
                                            <div className="col-2 div-group-icon">
                                                <div class="form-group form-group-icon">
                                                    <img src={GroupIcon} className="CheckGroupIcon" />
                                                </div>
                                            </div>
                                            <div className="col-10">
                                                <Checkbox className="colum-chooser-check-box" defaultChecked={true} label={"Name of the column"} />
                                            </div>
                                        </div>
                                        <div className="row row-check-box">
                                            <div className="col-2 div-group-icon">
                                                <div class="form-group form-group-icon">
                                                    <img src={GroupIcon} className="CheckGroupIcon" />
                                                </div>
                                            </div>
                                            <div className="col-10">
                                                <Checkbox defaultChecked={true} className="colum-chooser-check-box" label={"Name of the column"} />
                                            </div>
                                        </div>
                                        <div className="row row-check-box">
                                            <div className="col-2 div-group-icon">
                                                <div class="form-group form-group-icon">
                                                    <img src={GroupIcon} className="CheckGroupIcon" />
                                                </div>
                                            </div>
                                            <div className="col-10">
                                                <Checkbox defaultChecked={true} className="colum-chooser-check-box" label={"Name of the column"} />
                                            </div>
                                        </div>
                                        <div className="row row-check-box">
                                            <div className="col-2 div-group-icon">
                                                <div class="form-group form-group-icon">
                                                    <img src={GroupIcon} className="CheckGroupIcon" />
                                                </div>
                                            </div>
                                            <div className="col-10">
                                                <Checkbox defaultChecked={true} className="colum-chooser-check-box" label={"Name of the column"} />
                                            </div>
                                        </div>
                                        <div className="row row-check-box">
                                            <div className="col-2 div-group-icon">
                                                <div class="form-group form-group-icon">
                                                    <img src={GroupIcon} className="CheckGroupIcon" />
                                                </div>
                                            </div>
                                            <div className="col-10">
                                                <Checkbox defaultChecked={true} className="colum-chooser-check-box" label={"Name of the column"} />
                                            </div>
                                        </div>
                                        <Divider className={classes.divider} />
                                        <div className="row row-bottom">
                                            <div className="col-12 row-col-md">
                                                <label for="fname">SELECT COLUMNS</label>
                                            </div>
                                        </div>
                                        <div className="row row-check-box">
                                            <div className="col-12">
                                                <Checkbox className="colum-chooser-check-box" label={"Name of the column"} />
                                            </div>
                                        </div>
                                        <div className="row row-check-box">
                                            <div className="col-12">
                                                <Checkbox className="colum-chooser-check-box" label={"Name of the column"} />
                                            </div>
                                        </div>
                                        <div className="row row-check-box">
                                            <div className="col-12">
                                                <Checkbox className="colum-chooser-check-box" label={"Name of the column"} />
                                            </div>
                                        </div>
                                        <div className="row row-check-box">
                                            <div className="col-12">
                                                <Checkbox className="colum-chooser-check-box" label={"Name of the column"} />
                                            </div>
                                        </div>
                                        <div className="row row-check-box">
                                            <div className="col-12">
                                                <Checkbox className="colum-chooser-check-box" label={"Name of the column"} />
                                            </div>
                                        </div>
                                        <div className="row row-check-box">
                                            <div className="col-12">
                                                <Checkbox className="colum-chooser-check-box" label={"Name of the column"} />
                                            </div>
                                        </div>
                                        <div className="row row-check-box">
                                            <div className="col-12">
                                                <Checkbox className="colum-chooser-check-box" label={"Name of the column"} />
                                            </div>
                                        </div>
                                        <div className="row row-check-box">
                                            <div className="col-12">
                                                <Checkbox className="colum-chooser-check-box" label={"Name of the column"} />
                                            </div>
                                        </div>
                                        <div className="row row-check-box">
                                            <div className="col-12">
                                                <Checkbox className="colum-chooser-check-box" label={"Name of the column"} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="model-bottom">
                                        <input type="submit" value="SAVE"></input>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div
                            className={`columnOverlay ${displayColumnModal ? "Show" : ""}`}
                            onClick={() => setColumnDisplayModal(!displayColumnModal)}
                        />
                    </div>
                    {
                        <Grid data={dataState.map(item => ({
                            ...item,
                            [SELECTED_FIELD]: selectedState[idGetter(item)]
                        }))} style={{
                            height: "100%",


                        }} dataItemKey={DATA_ITEM_KEY} selectedField={SELECTED_FIELD} selectable={{
                            enabled: true,
                            drag: false,
                            cell: false,
                            mode: 'multiple'
                        }} onSelectionChange={onSelectionChange} onHeaderSelectionChange={onHeaderSelectionChange}>

                            <GridColumn
                                box-sizing="border-box"
                                width="22px"
                                background-color="#FFF"
                                border="1px solid #C4C4C4"
                                cell={(props) => (
                                    <td className="CheckBoxInput">
                                        <img src={GroupIcon} className="GroupIcon" />
                                        &nbsp;
                                    </td>
                                )}
                            />

                            <GridColumn field={SELECTED_FIELD} width="30px"
                                headerSelectionValue={dataState.findIndex(item => !selectedState[idGetter(item)]) === -1}
                            />

                            <GridColumn
                                field="Sortrder"
                                title="Sort Order"
                                width="55px"
                                color="#595959"
                                font-size="14px"
                                font-weight="500"
                                line-height="16px"
                                cell={(props) => (
                                    <td className="Sortrder">
                                        <select id="select-box">
                                            <img className="down-arrow" />
                                            <option selected>1.1</option>
                                            <option>1.2</option>
                                            <option>1.3</option>
                                            <option>1.4</option>
                                            <option>1.5</option>
                                            <option>1.6</option>
                                            <option>1.7</option>
                                            <option>1.8</option>
                                            <option>1.9</option>
                                        </select>
                                    </td>
                                )}
                            />

                            <GridColumn
                                field="Image"
                                width="60px"
                                color="#595959"
                                font-size="14px"
                                font-weight="500"
                                line-height="16px"
                                cell={(props) => (
                                    <td>
                                        <img src={imagePath} className="Img" />
                                    </td>
                                )}
                            />
                            <GridColumn
                                title="Part #"
                                field="Part"
                                width="95px"
                                color="#595959"
                                font-size="14px"
                                font-weight="500"
                                line-height="16px"

                                cell={(props) => (
                                    <td className="part">
                                        {props.dataItem[props.field || ""]}
                                    </td>
                                )}
                            />
                            <GridColumn
                                title="Description"
                                field="Description"
                                width="300px"
                                color="#595959"
                                font-size="14px"
                                font-weight="500"
                                line-height="16px"
                                cell={(props) => (
                                    <td className="Description">
                                        {props.dataItem[props.field || ""]}
                                    </td>
                                )}
                            />
                            <GridColumn
                                title="Mfr. Name"
                                field="Mfr"
                                width="65px"
                                color="#595959"
                                font-size="14px"
                                font-weight="500"
                                line-height="16px"
                                cell={(props) => (
                                    <td className="Mfr">
                                        {props.dataItem[props.field || ""]}
                                    </td>
                                )}
                            />
                            <GridColumn
                                title="Price"
                                field="Price"
                                width="60px"
                                color="#595959"
                                font-size="14px"
                                font-weight="500"
                                line-height="16px"
                                cell={(props) => (
                                    <td className="Price">
                                        {props.dataItem[props.field || ""]}
                                    </td>
                                )}
                            />
                            <GridColumn
                                title="Catalog"
                                field="Catalog"
                                width="60px"
                                color="#595959"
                                font-size="14px"
                                font-weight="500"
                                line-height="16px"
                                cell={(props) => (
                                    <td className="Catalog">
                                        {props.dataItem[props.field || ""]}
                                    </td>
                                )}
                            /><GridColumn
                                title="Entered By"
                                field="EnteredBy"
                                width="70px"
                                color="#595959"
                                font-size="14px"
                                font-weight="500"
                                line-height="16px"
                                cell={(props) => (
                                    <td className="EnteredBy">
                                        {props.dataItem[props.field || ""]}
                                    </td>
                                )}
                            />
                            <GridColumn
                                title="Setting"
                                field="Setting"
                                headerCell={SettingIcon}
                                width="35px"

                                cell={(props) => (
                                    <td className="sty_sub_set">
                                        <Menu hoverCloseDelay={50000} onSelect={() => setDisplayModal(!displayModal)}>
                                            <MenuItem icon="more-horizontal">
                                                <MenuItem text="Edit" />
                                                <MenuItem text="Move Up" />
                                                <MenuItem text="Move Down" />
                                                <MenuItem text="Change Group" />
                                                <MenuItem text="Remove" />
                                            </MenuItem>
                                        </Menu>
                                        {/* <Menu items={Sortitems} hoverCloseDelay={50000} openOnClick={true} className="Sort" /> */}
                                        {/* <DropDownButton icon="more-horizontal" className="edtihorizontal" onItemClick={() => setDisplayModal(!displayModal)} look="flat">
                                            <DropDownButtonItem text="Edit" />
                                            <DropDownButtonItem text="Move Up" />
                                            <DropDownButtonItem text="Move Down" />
                                            <DropDownButtonItem text="Change Group" />
                                            <DropDownButtonItem text="Remove" />
                                        </DropDownButton> */}
                                    </td>
                                )}
                            />
                        </Grid>
                    }
                </div>
            </div >

        </div >
    );
}
export default HomePage